package com.id.api.event

import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.datatype.jsr310.ser.ZonedDateTimeSerializer
import java.time.LocalDateTime

class Event<K, T> (
    val eventType: Type? = null,
    val key: K? = null,
    val data: T? = null,
    eventCreatedAt: LocalDateTime? = null
) {
    enum class Type {
        CREATE, DELETE
    }

    val eventCreatedAt = eventCreatedAt
        @JsonSerialize(using = ZonedDateTimeSerializer::class) get
}
