package com.id.api.composite.product

data class ServiceAddresses(
    val cmp: String? = null,
    val pro: String? = null,
    val rev: String? = null,
    val rec: String? = null,
)
