package com.id.api.composite.product

data class ProductAggregate(
    val productId: Int,
    val name: String,
    val weight: Int,
    val recommendations: List<RecommendationSummary>? = null,
    val reviews: List<ReviewSummary>? = null,
    val serviceAddresses: ServiceAddresses? = null
)
