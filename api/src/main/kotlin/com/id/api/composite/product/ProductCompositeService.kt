package com.id.api.composite.product

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import reactor.core.publisher.Mono

@Tag(name = "ProductComposite", description = "REST API for composite product information")
interface ProductCompositeService {

    /**
     * Sample usage:
     *
     * curl -X POST $HOST:$PORT/product-composite \
     * -H "Content-Type: application/json" --data \
     * '{"productId":123,"name":"product 123","weight":123}'
     *
     * @param body
     */
    @Operation(
        description = "\${api.product-composite.create-composite-product.description}",
        summary = "\${api.product-composite.create-composite-product.notes}"
    )
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "400",
                description = "Bad Request, invalid format of the request. See response message for more information."
            ), ApiResponse(
                responseCode = "422",
                description = "Unprocessable entity, input parameters caused the processing to fail. See response message for more information."
            )
        ]
    )
    @ResponseStatus(HttpStatus.ACCEPTED)
    @PostMapping(value = ["/product-composite"], consumes = ["application/json"])
    fun createProduct(@RequestBody body: ProductAggregate): Mono<Void>

    /**
     * Sample usage: curl $HOST:$PORT/product-composite/1
     *
     * @param productId
     * @return the composite product info, if found, else null
     */
    @Operation(
        summary = "\${api.product-composite.get-composite-product.description}",
        description = "\${api.product-composite.get-composite-product.notes}"
    )
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "200", description = "\${api.responseCodes.ok.description}"),
            ApiResponse(responseCode = "400", description = "\${api.responseCodes.badRequest.description}"),
            ApiResponse(responseCode = "404", description = "\${api.responseCodes.notFound.description}"),
            ApiResponse(responseCode = "422", description = "\${api.responseCodes.unprocessableEntity.description}")
        ]
    )
    @GetMapping(value = ["/product-composite/{productId}"], produces = ["application/json"])
    fun getProduct(
        @RequestHeader headers: HttpHeaders,
        @PathVariable productId: Int,
        @RequestParam(value = "delay", required = false) delay: Int? = 0,
        @RequestParam(value = "faultPercent", required = false) faultPercent: Int? = 0
    ): Mono<ProductAggregate>

    /**
     * Sample usage:
     *
     * curl -X DELETE $HOST:$PORT/product-composite/1
     *
     * @param productId
     */
    @Operation(
        description = "\${api.product-composite.delete-composite-product.description}",
        summary = "\${api.product-composite.delete-composite-product.notes}"
    )
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "400",
                description = "Bad Request, invalid format of the request. See response message for more information."
            ), ApiResponse(
                responseCode = "422",
                description = "Unprocessable entity, input parameters caused the processing to fail. See response message for more information."
            )
        ]
    )
    @ResponseStatus(HttpStatus.ACCEPTED)
    @DeleteMapping(value = ["/product-composite/{productId}"])
    fun deleteProduct(@PathVariable productId: Int): Mono<Void>
}
