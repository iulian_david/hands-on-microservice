package com.id.api.core.review

data class Review(
    val productId: Int = 0,
    val reviewId: Int,
    val author: String? = null,
    val subject: String? = null,
    val content: String? = null,
    val serviceAddress: String? = null
)
