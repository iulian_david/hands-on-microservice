package com.id.api.core.recommendation

import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestParam
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface RecommendationService {

    fun createRecommendation(@RequestBody body: Recommendation): Mono<Recommendation>

    /**
     * Sample usage: curl $HOST:$PORT/recommendation?productId=1
     *
     * @param productId
     * @return
     */
    @GetMapping(value = ["/recommendation"], produces = ["application/json"])
    fun getRecommendations(
        @RequestHeader headers: HttpHeaders,
        @RequestParam(value = "productId", required = true) productId: Int
    ): Flux<Recommendation>

    fun deleteRecommendations(@RequestParam(value = "productId", required = true) productId: Int): Mono<Void>
}
