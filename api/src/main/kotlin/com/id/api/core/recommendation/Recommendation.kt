package com.id.api.core.recommendation

data class Recommendation(
    val productId: Int = 0,
    val recommendationId: Int = 0,
    val author: String? = null,
    var rate: Int = 0,
    val content: String,
    var serviceAddress: String? = "Unknown Address"
)
