package com.id.api.core.product

data class Product(
    val productId: Int = 0,
    val name: String,
    val weight: Int,
    val serviceAddress: String? = null
)
