package com.id.api.core.review

import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestParam
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface ReviewService {

    fun createReview(@RequestBody body: Review): Mono<Review>

    /**
     * Sample usage: curl $HOST:$PORT/review?productId=1
     *
     * @param productId
     * @return
     */
    @GetMapping(value = ["/review"], produces = ["application/json"])
    fun getReviews(
        @RequestHeader headers: HttpHeaders,
        @RequestParam(value = "productId", required = true) productId: Int
    ): Flux<Review>

    fun deleteReviews(@RequestParam(value = "productId", required = true) productId: Int): Mono<Void>
}
