# TEST

## Create the Minikube cluster from scratch
```shell
kubectl delete ns hands-on
kubectl delete ns cert-manager
kubectl delete ns istio-system
kubectl delete ns hands-on
kubectl delete ns logging
minikube delete -p handson-spring-boot-cloud 
```

#### Build and test locally using Docker Compose before deploying to Kubernetes on Minikube
```shell
./gradlew clean build
docker compose build
# Verify on plain Docker
HOST=localhost PORT=8443 USE_K8S=false HEALTH_URL=https://127.0.0.1:8443 ./test-em-all.bash start stop
```

#### Test locally using Docker Compose and Kafka before deploying to Kubernetes on Minikube
```shell
docker compose --file=docker-compose-kafka.yml build
docker compose --file=docker-compose-kafka.yml up -d
# Verify on plain Docker
HOST=localhost PORT=8443 USE_K8S=false HEALTH_URL=https://127.0.0.1:8443 ./test-em-all.bash stop
```

#### Test locally using Docker Compose and RabbitMQ partitions before deploying to Kubernetes on Minikube
```shell
docker compose --file=docker-compose-partitions.yml build
docker compose --file=docker-compose-partitions.yml up -d
# Verify on plain Docker
HOST=localhost PORT=8443 USE_K8S=false HEALTH_URL=https://127.0.0.1:8443 ./test-em-all.bash stop
```

 # macOS-version
```shell
 minikube start \
 --profile=handson-spring-boot-cloud \
 --memory=10240 \
 --cpus=4 \
 --disk-size=30g \
 --kubernetes-version=v1.25.0 \
 --driver=hyperkit
 ```

 # Linux - version
```shell 
minikube start \
 --profile=handson-spring-boot-cloud \
 --memory=10240 \
 --cpus=4 \
 --disk-size=30g \
 --driver=docker \
 --ports=8080:80 --ports=443:443 \
 --ports=30080:30080 --ports=30443:30443
```

 # WSL2-version
```shell
 minikube start \
 --profile=handson-spring-boot-cloud \
 --memory=10240 \
 --cpus=4 \
 --disk-size=30g \
 --kubernetes-version=v1.25.0 \
 --driver=docker \
 --ports=8080:80 --ports=8443:443 \
 --ports=30080:30080 --ports=30443:30443
```
#### Install the cert-manager
```shell
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install cert-manager jetstack/cert-manager \
   --create-namespace \
   --namespace cert-manager \
   --set installCRDs=true \
   --wait
```
### Add istio
##### precheck first and then install it
```shell
istioctl experimental precheck
istioctl install --skip-confirmation \
  --set profile=demo \
  --set meshConfig.accessLogFile=/dev/stdout \
  --set meshConfig.accessLogEncoding=JSON \
  --set values.pilot.env.PILOT_JWT_PUB_KEY_REFRESH_INTERVAL=15s
kubectl -n istio-system wait --timeout=600s --for=condition=available deployment --all
istio_version=$(istioctl version --short --remote=false)
echo $istio_version
echo "Installing integrations for Istio v$istio_version"
echo "Install the extra components described in the Introducing Istio section – Kiali, Jaeger, Prometheus, and Grafana "
kubectl apply -n istio-system -f https://raw.githubusercontent.com/istio/istio/$istio_version/samples/addons/kiali.yaml
kubectl apply -n istio-system -f https://raw.githubusercontent.com/istio/istio/$istio_version/samples/addons/jaeger.yaml
kubectl apply -n istio-system -f https://raw.githubusercontent.com/istio/istio/$istio_version/samples/addons/prometheus.yaml
kubectl apply -n istio-system -f https://raw.githubusercontent.com/istio/istio/$istio_version/samples/addons/grafana.yaml
# install test mail server in Istio's Namespace
kubectl -n istio-system create deployment mail-server --image maildev/maildev:1.1.0
kubectl -n istio-system expose deployment mail-server --port=80,25 --type=ClusterIP
kubectl -n istio-system wait --timeout=60s --for=condition=ready pod -l app=mail-server
# make the mail server's web UI available from the outside of Minikube
helm upgrade istio-hands-on-addons kubernetes/helm/environments/istio-system -n istio-system
echo "Wait for all deployments to be up"
kubectl -n istio-system wait --timeout=600s --for=condition=available deployment --all
```
#### Setting up access to Istio services(from with the project folder)
```shell
helm upgrade --install istio-hands-on-addons kubernetes/helm/environments/istio-system -n istio-system --wait
echo "check secret"
kubectl -n istio-system get secrets hands-on-certificate
echo "check certificate"
kubectl -n istio-system get certificate hands-on-certificate
```

### First, remove an existing entry, if any:
```shell
sudo sed -i.bak '/minikube.me/d' /etc/hosts
```

### Add the new entry of minikube.me in /etc/hosts
##### in another terminal start the tunnel
```shell
minikube profile handson-spring-boot-cloud
minikube tunnel
```
#### macOS-version, linux
```shell
eval $(minikube docker-env)
INGRESS_IP=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
echo $INGRESS_IP
MINIKUBE_HOSTS="minikube.me grafana.minikube.me kiali.minikube.me prometheus.minikube.me tracing.minikube.me kibana.minikube.me elasticsearch.minikube.me mail.minikube.me health.minikube.me elasticsearch.minikube.me kibana.minikube.me mail.minikube.me"
echo "$INGRESS_IP $MINIKUBE_HOSTS" | sudo tee -a /etc/hosts
```

#### WSL2-version
```shell
sudo bash -c "echo 127.0.0.1 minikube.me | tee -a /etc/hosts"
```

#### Add ingress
```shell
 minikube profile handson-spring-boot-cloud
 minikube addons enable ingress
 minikube addons enable metrics-server

 kubectl wait --timeout=600s --for=condition=ready pod -n kube-system --all
```
#### Validate hostname changes

```shell
curl -o /dev/null -sk -L -w "%{http_code}\n" https://kiali.minikube.me/kiali/
curl -o /dev/null -sk -L -w "%{http_code}\n" https://tracing.minikube.me
curl -o /dev/null -sk -L -w "%{http_code}\n" https://grafana.minikube.me
curl -o /dev/null -sk -L -w "%{http_code}\n" https://prometheus.minikube.me/graph#/
```



#### Deploy the application to Kubernetes on Minikube
```shell
 eval $(minikube docker-env)
 docker build -f kubernetes/efk/Dockerfile -t hands-on/fluentd:v1 kubernetes/efk/
 docker compose build
 kubectl delete namespace hands-on
 kubectl apply -f kubernetes/hands-on-namespace.yml
 kubectl config set-context $(kubectl config current-context)
 docker pull mysql:8
 docker pull mongo:latest
 docker pull rabbitmq:3-management
 docker pull openzipkin/zipkin:latest
 docker pull docker.elastic.co/elasticsearch/elasticsearch:7.12.1
 docker pull docker.elastic.co/kibana/kibana:7.12.1
 kubectl apply -f kubernetes/efk/fluentd-hands-on-configmap.yml 
 kubectl apply -f kubernetes/efk/fluentd-ds.yml
 kubectl wait --timeout=120s --for=condition=Ready pod -l app=fluentd -n kube-system
 kubectl logs -n kube-system -l app=fluentd --tail=-1 | grep "fluentd worker is now running worker"
 echo "Resolve the Helm chart dependencies with the following commands:"
 eval $(minikube docker-env -u)

 for f in kubernetes/helm/components/*; do helm dep up $f; done
 for f in kubernetes/helm/environments/*; do helm dep up $f; done
echo "Deploy the system landscape using Helm and wait for all Deployments to complete"
helm install hands-on-dev-env \
kubernetes/helm/environments/dev-env \
-n hands-on \
--wait

# ADD logging environment
helm install logging-hands-on-add-on kubernetes/helm/environments/logging \
    -n logging --create-namespace --wait
kubectl -n hands-on wait --timeout=600s --for=condition=ready pod --all

# Verify that Elasticsearch is up and running
curl https://elasticsearch.minikube.me -sk | jq -r .tagline
```

#### Configure Grafana to send emails to the test mail server
```shell
kubectl -n istio-system set env deployment/grafana \
    GF_SMTP_ENABLED=true \
    GF_SMTP_SKIP_VERIFY=true \
    GF_SMTP_HOST=mail-server:25 \
    GF_SMTP_FROM_ADDRESS=grafana@minikube.me
kubectl -n istio-system wait --timeout=60s --for=condition=ready pod -l app=Grafana

```
### Run the test
```shell
./test-em-all.bash
```
### Siege test
#### Install siege, check config:
```
edit ~/.siege/siege.conf 
#change the json_output to false
json_output = false
```
```shell
echo "the writer secret is the same as in test-em-all.bash as in authorization-server->SecurityConfig.kt:95 "
ACCESS_TOKEN=$(curl -k https://writer:4031da8a-2de5-4f25-bcb5-d403bb41c92f@minikube.me/oauth2/token -d grant_type=client_credentials -s | jq .access_token -r)

echo ACCESS_TOKEN=$ACCESS_TOKEN

siege https://minikube.me/product-composite/1 -H "Authorization: Bearer $ACCESS_TOKEN" -c1 -d1 -v
```

#### Test the circuit breaker
##### Start the circuit breaker
```shell
ACCESS_TOKEN=$(curl -k https://writer:4031da8a-2de5-4f25-bcb5-d403bb41c92f@minikube.me/oauth2/token -d grant_type=client_credentials -s | jq .access_token -r)
echo ACCESS_TOKEN=$ACCESS_TOKEN
for ((n=0; n<4; n++)); do curl -o /dev/null -skL -w "%{http_code}\n" https://minikube.me/product-composite/1?delay=3 -H "Authorization: Bearer $ACCESS_TOKEN" -s; done
```

##### Stop the circuit breaker
```shell
ACCESS_TOKEN=$(curl -k https://writer:4031da8a-2de5-4f25-bcb5-d403bb41c92f@minikube.me/oauth2/token -d grant_type=client_credentials -s | jq .access_token -r)
echo ACCESS_TOKEN=$ACCESS_TOKEN
for ((n=0; n<4; n++)); do curl -o /dev/null -skL -w "%{http_code}\n" https://minikube.me/product-composite/1?delay=0 -H "Authorization: Bearer $ACCESS_TOKEN" -s; done
```



