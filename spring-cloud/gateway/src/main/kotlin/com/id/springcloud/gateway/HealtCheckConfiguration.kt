package com.id.springcloud.gateway

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.actuate.health.CompositeReactiveHealthContributor
import org.springframework.boot.actuate.health.Health
import org.springframework.boot.actuate.health.ReactiveHealthContributor
import org.springframework.boot.actuate.health.ReactiveHealthIndicator
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import java.util.logging.Level


@Configuration
class HealthCheckConfiguration(
    @Autowired private val webClientBuilder: WebClient.Builder
) {

    private val webClient: WebClient = webClientBuilder.build()


    @Bean
    fun healthcheckMicroservices(): ReactiveHealthContributor {
        val registry: MutableMap<String, ReactiveHealthIndicator> = LinkedHashMap()
        registry["product"] = ReactiveHealthIndicator { getHealth("http://product:4004") }
        registry["recommendation"] = ReactiveHealthIndicator { getHealth("http://recommendation:4004") }
        registry["review"] = ReactiveHealthIndicator { getHealth("http://review:4004") }
        registry["product-composite"] = ReactiveHealthIndicator { getHealth("http://product-composite:4004") }
        return CompositeReactiveHealthContributor.fromMap(registry)
    }

    private fun getHealth(baseUrl: String): Mono<Health> {
        val url = "$baseUrl/actuator/health"
        LOG.debug("Setting up a call to the Health API on URL: {}", url)
        return webClient.get().uri(url).retrieve().bodyToMono(String::class.java)
            .map { Health.Builder().up().build() }
            .onErrorResume { ex ->
                Mono.just(Health.Builder().down(ex).build())
            }
            .log(LOG.name, Level.FINE)
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(HealthCheckConfiguration::class.java)
    }

}
