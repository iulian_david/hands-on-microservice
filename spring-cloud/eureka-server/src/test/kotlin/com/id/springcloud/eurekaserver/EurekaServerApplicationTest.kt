package com.id.springcloud.eurekaserver

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpStatus

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
properties = ["management.health.rabbit.enabled=false", "spring.cloud.config.enabled=false"])
internal class EurekaServerApplicationTest(
    @Autowired private val testRestTemplate: TestRestTemplate,
    @Autowired @Value("\${app.eureka-username}") private val username: String,
    @Autowired @Value("\${app.eureka-password}") private val password: String
) {


    @Test
    fun catalogLoads() {
        val expectedReponseBody =
            "{\"applications\":{\"versions__delta\":\"1\",\"apps__hashcode\":\"\",\"application\":[]}}"
        val entity = testRestTemplate
            .withBasicAuth(username, password)
            .getForEntity("/eureka/apps", String::class.java)
        assertEquals(HttpStatus.OK, entity.statusCode)
        assertEquals(expectedReponseBody, entity.body)
    }

    @Test
    fun healthy() {
        val expectedReponseBody = "{\"status\":\"UP\"}"
        val entity = testRestTemplate
            .withBasicAuth(username, password)
            .getForEntity(
            "/actuator/health",
            String::class.java
        )
        assertEquals(HttpStatus.OK, entity.statusCode)
        assertEquals(expectedReponseBody, entity.body)
    }
}