package com.id.springcloud.configserver

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.config.server.EnableConfigServer

@EnableConfigServer
@SpringBootApplication
class ConfigServerApplication {
    companion object {
        val LOG: Logger =
            LoggerFactory.getLogger(ConfigServerApplication::class.java)
    }
}

fun main(args: Array<String>) {
    SpringApplication.run(ConfigServerApplication::class.java, *args)
        .also {
            val repoLocation = it.environment.getProperty("spring.cloud.config.server.native.searchLocations")
            ConfigServerApplication.LOG.info("Serving configurations from folder: $repoLocation")
        }


}