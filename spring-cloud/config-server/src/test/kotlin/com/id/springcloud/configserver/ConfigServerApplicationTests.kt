package com.id.springcloud.configserver

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, properties = ["spring.profiles.active=native"])
internal class ConfigServerApplicationTests {

    @Test
    fun contextLoads() {
    }
}