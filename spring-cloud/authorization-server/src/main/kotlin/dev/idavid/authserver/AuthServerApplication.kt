package dev.idavid.authserver

import org.springframework.boot.ApplicationRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.security.crypto.factory.PasswordEncoderFactories

@SpringBootApplication
class AuthServerApplication {
    @Bean
    fun runner() = ApplicationRunner {
        val encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder()
        // outputs {bcrypt}$2a$10$dXJ3SW6G7P50lGmMkkmwe.20cQQubK3.HZWzG3YB1tlRy.fqvM/BG
        // remember the password that is printed out and use in the next step
//        println(encoder.encode("p"))
    }
}

fun main(args: Array<String>) {
    runApplication<AuthServerApplication>(*args)
}
