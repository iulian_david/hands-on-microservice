rootProject.name = "Hands On microservices"
include(
    ":api", ":util",
    ":microservices:product-service",
    ":microservices:review-service",
    ":microservices:recommendation-service",
    ":microservices:product-composite-service",
    ":spring-cloud:gateway",
    ":spring-cloud:authorization-server",
    ":spring-cloud:auth-server_old"
)

