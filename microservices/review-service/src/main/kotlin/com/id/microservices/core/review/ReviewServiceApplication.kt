package com.id.microservices.core.review

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import reactor.core.scheduler.Scheduler
import reactor.core.scheduler.Schedulers

@SpringBootApplication
@ComponentScan("com.id")
class ReviewServiceApplication(
    @Autowired @Value("\${app.threadPoolSize:10}") val threadPoolSize: Int,
    @Autowired @Value("\${app.taskQueueSize:100}") val taskQueueSize: Int
) {
    companion object {
        private val LOG = LoggerFactory.getLogger(ReviewServiceApplication::class.java)
    }

    @Bean
    fun jdbcScheduler(): Scheduler {
        LOG.info("Creates a jdbcScheduler with threadPoolSize = $threadPoolSize and taskQueueSize = $taskQueueSize")
        return Schedulers.newBoundedElastic(threadPoolSize, taskQueueSize, "jdbc-pool")
    }
}

fun main(args: Array<String>) {
    runApplication<ReviewServiceApplication>(*args)
}
