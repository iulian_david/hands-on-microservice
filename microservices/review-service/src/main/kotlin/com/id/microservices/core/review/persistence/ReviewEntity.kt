package com.id.microservices.core.review.persistence

import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import jakarta.persistence.Index
import jakarta.persistence.Table
import jakarta.persistence.Version

@Entity
@Table(
    name = "reviews",
    indexes = [
        Index(
            name = "reviews_unique_idx",
            unique = true,
            columnList = "productId,reviewId"
        )
    ]
)
data class ReviewEntity(
    @Id @GeneratedValue val id: Int = 0,
    @Version val version: Int = 0,
    val productId: Int,
    val reviewId: Int,
    val author: String,
    val subject: String,
    val content: String
)
