package com.id.microservices.core.review.services

import com.id.api.core.review.Review
import com.id.api.core.review.ReviewService
import com.id.api.exceptions.InvalidInputException
import com.id.microservices.core.review.persistence.ReviewRepository
import com.id.util.http.ServiceUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.scheduler.Scheduler
import reactor.kotlin.core.publisher.toFlux
import java.util.logging.Level

@RestController
class ReviewServiceImpl(
    @Autowired private val serviceUtil: ServiceUtil,
    @Autowired private val reviewMapper: ReviewMapper,
    @Autowired private val repository: ReviewRepository,
    @Qualifier("jdbcScheduler") val jdbcScheduler: Scheduler
) : ReviewService {
    companion object {
        private val LOG: Logger = LoggerFactory.getLogger(ReviewServiceImpl::class.java)
    }

    override fun createReview(body: Review): Mono<Review> =
        Mono.fromCallable {
            internalCreateReview(body)
        }.subscribeOn(jdbcScheduler)

    private fun internalCreateReview(body: Review): Review =
        try {
            reviewMapper.apiToEntity(body)
                .let { repository.save(it) }
                .let { reviewMapper.entityToApi(it) }
                .also {
                    LOG.debug("createReview: created a review entity: {}/{}", body.productId, body.reviewId)
                }
        } catch (e: DataIntegrityViolationException) {
            throw InvalidInputException(
                "Duplicate key, Product Id: ${body.productId}, Review Id:${body.reviewId}"
            )
        }

    override fun getReviews(headers: HttpHeaders, productId: Int): Flux<Review> =
        when {
            productId < 1 -> {
                throw InvalidInputException("Invalid productId: $productId")
            }
            else -> {
                val serviceAddress = serviceUtil.serviceAddress
                Mono.fromCallable {
                    repository.findByProductId(productId)
                        .let { reviewMapper.entityListToApiList(it) }
                        .map { r -> r.copy(serviceAddress = serviceAddress) }
                        .also {
                            LOG.debug("/reviews response size: {}", it.size)
                        }
                }.flatMapMany { it.toFlux() }
                    .log(LOG.name, Level.FINE)
                    .subscribeOn(jdbcScheduler)
            }
        }

    override fun deleteReviews(productId: Int): Mono<Void> =
        when {
            productId < 1 -> {
                throw InvalidInputException("Invalid productId: $productId")
            }
            else -> {
                LOG.debug("deleteReviews: tries to delete reviews for the product with productId: {}", productId)
                Mono.fromCallable {
                    repository.deleteAll(repository.findByProductId(productId))
                }
                    .subscribeOn(jdbcScheduler)
                    .then()
            }
        }
}
