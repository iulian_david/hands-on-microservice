package com.id.microservices.core.review.services

import com.id.api.core.review.Review
import com.id.microservices.core.review.persistence.ReviewEntity
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.mapstruct.factory.Mappers

class ReviewMapperTests {

    private val mapper = Mappers.getMapper(ReviewMapper::class.java)

    @Test
    fun mapperTests() {
        assertNotNull(mapper)
        val api = Review(1, 2, "a", "s", "C", "adr")

        val entity: ReviewEntity = mapper.apiToEntity(api)
        assertThat(entity).usingRecursiveComparison()
            .ignoringFields("id", "version")
            .isEqualTo(api)

        val api2: Review = mapper.entityToApi(entity)
        assertThat(api2).usingRecursiveComparison()
            .ignoringFields("serviceAddress")
            .isEqualTo(api)
        assertNull(api2.serviceAddress)
    }

    @Test
    fun mapperListTests() {
        assertNotNull(mapper)
        val api = Review(1, 2, "a", "s", "C", "adr")
        val apiList: List<Review> = listOf(api)
        val entityList: List<ReviewEntity> = mapper.apiListToEntityList(apiList)

        assertThat(apiList)
            .usingRecursiveComparison()
            .ignoringFields("id", "version", "serviceAddress")
            .isEqualTo(entityList)

        val api2List: List<Review> = mapper.entityListToApiList(entityList)
        assertThat(apiList)
            .usingRecursiveComparison()
            .ignoringFields("serviceAddress")
            .isEqualTo(apiList)
        assertNull(api2List[0].serviceAddress)
    }
}
