package com.id.microservices.core.review

import com.id.api.core.review.Review
import com.id.api.event.Event
import com.id.api.exceptions.InvalidInputException
import com.id.microservices.core.review.persistence.MySqlTestBase
import com.id.microservices.core.review.persistence.ReviewRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import java.util.function.Consumer

@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    properties = [
        "spring.cloud.stream.defaultBinder=rabbit",
        "logging.level.com.id=DEBUG",
        "spring.jpa.hibernate.ddl-auto=update"
    ]
)
class ReviewServiceApplicationTests : MySqlTestBase() {

    @Autowired
    private lateinit var client: WebTestClient

    @Autowired
    private lateinit var repository: ReviewRepository

    @Autowired
    @Qualifier("messageProcessor")
    private lateinit var messageProcessor: Consumer<Event<Int, Review>>

    @BeforeEach
    fun setupDb() {
        repository.deleteAll()
    }

    @Test
    fun `get reviews by productId`() {
        val productId = 1
        assertThat(repository.findByProductId(productId)).hasSize(0)

        sendCreateReviewEvent(productId, 1)
        sendCreateReviewEvent(productId, 2)
        sendCreateReviewEvent(productId, 3)

        assertThat(repository.findByProductId(productId)).hasSize(3)

        getAndVerifyReviewsByProductId(productId, HttpStatus.OK)
            .jsonPath("$.length()").isEqualTo(3)
            .jsonPath("$[2].productId").isEqualTo(productId)
            .jsonPath("$[2].reviewId").isEqualTo(3)
    }

    @Test
    fun duplicateError() {
        val productId = 1
        val reviewId = 1
        assertEquals(0, repository.count())

        sendCreateReviewEvent(productId, reviewId)

        assertEquals(1, repository.count())
        assertThrows<InvalidInputException>("Expected a MessagingException here!") {
            sendCreateReviewEvent(productId, reviewId)
        }.also {
            assertEquals("Duplicate key, Product Id: 1, Review Id:1", it.message)
        }

        assertEquals(1, repository.count())
    }

    @Test
    fun deleteReviews() {
        val productId = 1
        val reviewId = 1
        sendCreateReviewEvent(productId, reviewId)
        assertThat(repository.findByProductId(productId)).hasSize(1)

        sendDeleteReviewEvent(productId)
        assertThat(repository.findByProductId(productId)).hasSize(0)

        sendDeleteReviewEvent(productId)
    }

    @Test
    fun getReviewsMissingParameter() {
        getAndVerifyReviewsByProductId("", HttpStatus.BAD_REQUEST)
            .jsonPath("$.path").isEqualTo("/review")
            .jsonPath("$.message").isEqualTo("Required query parameter 'productId' is not present.")
    }

    @Test
    fun getReviewsInvalidParameter() {
        getAndVerifyReviewsByProductId("?productId=no-integer", HttpStatus.BAD_REQUEST)
            .jsonPath("$.path").isEqualTo("/review")
            .jsonPath("$.message").isEqualTo("Type mismatch.")
    }

    @Test
    fun getReviewsNotFound() {
        getAndVerifyReviewsByProductId("?productId=213", HttpStatus.OK)
            .jsonPath("$.length()").isEqualTo(0)
    }

    @Test
    fun getReviewsInvalidParameterNegativeValue() {
        val productIdInvalid = -1
        getAndVerifyReviewsByProductId("?productId=$productIdInvalid", HttpStatus.UNPROCESSABLE_ENTITY)
            .jsonPath("$.path").isEqualTo("/review")
            .jsonPath("$.message").isEqualTo("Invalid productId: $productIdInvalid")
    }

    private fun getAndVerifyReviewsByProductId(productId: Int, expectedStatus: HttpStatus) =
        getAndVerifyReviewsByProductId("?productId=$productId", expectedStatus)

    private fun getAndVerifyReviewsByProductId(
        productIdQuery: String,
        expectedStatus: HttpStatus
    ) = client.get()
        .uri("/review$productIdQuery")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus().isEqualTo(expectedStatus)
        .expectHeader().contentType(MediaType.APPLICATION_JSON)
        .expectBody()

    private fun sendCreateReviewEvent(productId: Int, reviewId: Int) {
        val review = Review(
            productId, reviewId,
            "Author $reviewId", "Subject $reviewId", "Content $reviewId", "SA"
        )
        val event = Event(Event.Type.CREATE, productId, review)
        messageProcessor.accept(event)
    }

    private fun sendDeleteReviewEvent(productId: Int) {
        val event: Event<Int, Review> = Event(Event.Type.DELETE, productId, null)
        messageProcessor.accept(event)
    }
}
