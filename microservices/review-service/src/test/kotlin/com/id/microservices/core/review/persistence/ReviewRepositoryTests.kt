package com.id.microservices.core.review.persistence

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.fail
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.dao.OptimisticLockingFailureException
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional

@DataJpaTest(properties = [ "spring.jpa.hibernate.ddl-auto=update"])
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Transactional(propagation = Propagation.NOT_SUPPORTED)
internal class ReviewRepositoryTests : MySqlTestBase() {

    private lateinit var savedEntity: ReviewEntity

    @Autowired
    private lateinit var repository: ReviewRepository

    @BeforeEach
    fun setup() {
        repository.deleteAll()
        val entity = ReviewEntity(productId = 1, reviewId = 2, author = "a", subject = "s", content = "c")
        savedEntity = repository.save(entity)
        assertEqualsReview(entity, savedEntity)
        assertThat(savedEntity.id).isGreaterThan(0)
    }

    @Test
    fun create() {
        val newEntity = ReviewEntity(productId = 1, reviewId = 3, author = "a", subject = "s", content = "c")
        repository.save(newEntity)
        val foundEntity = repository.findById(newEntity.id).get()
        assertEqualsReview(newEntity, foundEntity)
        assertEquals(2, repository.count())
    }

    @Test
    fun update() {
        assertEquals(0, savedEntity.version.toLong())
        savedEntity.copy(author = "a2")
            .let {
                repository.save(it)
            }
        val (_, version, _, _, author) = repository.findById(savedEntity.id).get()
        assertEquals(1, version.toLong())
        assertEquals("a2", author)
    }

    @Test
    fun delete() {
        repository.delete(savedEntity)
        assertFalse(repository.existsById(savedEntity.id))
    }

    @Test
    fun getByProductId() {
        val entityList = repository.findByProductId(savedEntity.productId)
        assertThat(entityList).hasSize(1)
        assertEqualsReview(savedEntity, entityList[0])
    }

    @Test
    fun duplicateError() {
        val entity = ReviewEntity(productId = 1, reviewId = 2, author = "a", subject = "s", content = "c")
        assertThrows<DataIntegrityViolationException> {
            repository.save(entity)
        }
    }

    @Test
    fun optimisticLockError() {

        // Store the saved entity in two separate entity objects
        val entity1 = repository.findById(savedEntity.id).get()
        val entity2 = repository.findById(savedEntity.id).get()

        // Update the entity using the first entity object
        entity1.copy(author = "a1")
            .let {
                repository.save(it)
            }

        //  Update the entity using the second entity object.
        // This should fail since the second entity now holds a old version number, i.e. a Optimistic Lock Error
        try {
            entity2.copy(author = "a2")
                .let {
                    repository.save(it)
                }

            fail("Expected an OptimisticLockingFailureException")
        } catch (e: OptimisticLockingFailureException) {
        }

        // Get the updated entity from the database and verify its new sate
        val (_, version, _, _, author) = repository.findById(savedEntity.id).get()
        assertEquals(1, version)
        assertEquals("a1", author)
    }

    private fun assertEqualsReview(expectedEntity: ReviewEntity, actualEntity: ReviewEntity) {
//        assertEquals(expectedEntity.id, actualEntity.id)
        assertEquals(expectedEntity.version, actualEntity.version)
        assertEquals(expectedEntity.productId, actualEntity.productId)
        assertEquals(expectedEntity.reviewId, actualEntity.reviewId)
        assertEquals(expectedEntity.author, actualEntity.author)
        assertEquals(expectedEntity.subject, actualEntity.subject)
        assertEquals(expectedEntity.content, actualEntity.content)
    }
}
