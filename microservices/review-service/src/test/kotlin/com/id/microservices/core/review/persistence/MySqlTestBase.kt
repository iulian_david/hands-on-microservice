package com.id.microservices.core.review.persistence
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.MySQLContainer

abstract class MySqlTestBase {

    companion object {
        const val MYSQL_VERSION = "mysql:5.7.32"
        private val database: MySQLContainer<*> = MySQLContainer<Nothing>(MYSQL_VERSION)

        @DynamicPropertySource
        @JvmStatic
        fun databaseProperties(registry: DynamicPropertyRegistry) {
            registry.add("spring.datasource.url") { database.jdbcUrl }
            registry.add("spring.datasource.username") { database.username }
            registry.add("spring.datasource.password") { database.password }
        }
    }

    init {
        database.start()
    }
}
