package com.id.microservices.core.product.services

import com.id.api.core.product.Product
import com.id.api.core.product.ProductService
import com.id.api.event.Event
import com.id.api.exceptions.EventProcessingException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.function.Consumer

@Configuration
class MessageProcessorConfig(
    private val productService: ProductService
) {
    companion object {
        private val LOG: Logger =
            LoggerFactory.getLogger(MessageProcessorConfig::class.java)
    }

    @Bean
    fun messageProcessor(): Consumer<Event<Int, Product>> {
        return Consumer { event ->
            LOG.info(
                "Process message created at {}...",
                event.eventCreatedAt
            )
            when (event.eventType) {
                Event.Type.CREATE -> {
                    val product: Product = event.data ?: error("Invalid input")
                    LOG.info("Create product with ID: {}", product.productId)
                    productService.createProduct(product).block()
                }
                Event.Type.DELETE -> {
                    val productId: Int = event.key ?: error("Invalid id")
                    LOG.info("Delete product with ProductID: {}", productId)
                    productService.deleteProduct(productId).block()
                }
                else -> {
                    val errorMessage = "Incorrect event type: ${event.eventType}, expected a CREATE or DELETE event"
                    LOG.warn(errorMessage)
                    throw EventProcessingException(errorMessage)
                }
            }
            LOG.info("Message processing done!")
        }
    }
}
