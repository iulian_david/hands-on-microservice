package com.id.microservices.core.product.services

import com.id.api.core.product.Product
import com.id.api.core.product.ProductService
import com.id.api.exceptions.InvalidInputException
import com.id.api.exceptions.NotFoundException
import com.id.microservices.core.product.persistence.ProductEntity
import com.id.microservices.core.product.persistence.ProductRepository
import com.id.util.http.ServiceUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DuplicateKeyException
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import java.time.Duration
import kotlin.random.Random

@RestController
class ProductServiceImpl(
    @Autowired private val serviceUtil: ServiceUtil,
    @Autowired private val repository: ProductRepository,
    @Autowired private val mapper: ProductMapper
) : ProductService {

    companion object {
        private val LOG: Logger = LoggerFactory.getLogger(ProductServiceImpl::class.java)
    }

    override fun createProduct(body: Product): Mono<Product> =
        when {
            body.productId < 1 -> throw InvalidInputException("Invalid productId: " + body.productId)
            else -> {
                val entity: ProductEntity = mapper.apiToEntity(body)
                repository.save(entity)
                    .log()
                    .onErrorMap(
                        DuplicateKeyException::class.java
                    ) { InvalidInputException("Duplicate key, Product Id: " + body.productId) }
                    .map(mapper::entityToApi)
            }
        }

    override fun getProduct(
        headers: HttpHeaders,
        productId: Int,
        delay: Int,
        faultPercent: Int
    ): Mono<Product> =
        when {
            productId < 1 -> throw InvalidInputException("Invalid productId: $productId")
            else -> {
                repository.findByProductId(productId)
                    .map { throwErrorIfBadLuck(it, faultPercent) }
                    .delayElement(Duration.ofSeconds(delay.toLong()))
                    .switchIfEmpty(Mono.error(NotFoundException("No product found for productId: $productId")))
                    .log()
                    .map(mapper::entityToApi)
                    .map {
                        it.copy(
                            serviceAddress = serviceUtil.serviceAddress
                        )
                    }
            }
        }

    override fun deleteProduct(productId: Int): Mono<Void> {
        if (productId < 1) throw InvalidInputException("Invalid productId: $productId")
        LOG.debug("deleteProduct: tries to delete an entity with productId: {}", productId)
        return repository.findByProductId(productId)
            .log()
            .map(repository::delete)
            .flatMap { it }
    }

    private fun throwErrorIfBadLuck(entity: ProductEntity, faultPercent: Int): ProductEntity {
        if (faultPercent == 0) {
            return entity
        }
        val randomThreshold: Int = Random.nextInt(1, 100)
        if (faultPercent < randomThreshold) {
            LOG.debug("We got lucky, no error occurred, {} < {}", faultPercent, randomThreshold)
        } else {
            LOG.debug("Bad luck, an error occurred, {} >= {}", faultPercent, randomThreshold)
            throw RuntimeException("Something went wrong...")
        }
        return entity
    }
}
