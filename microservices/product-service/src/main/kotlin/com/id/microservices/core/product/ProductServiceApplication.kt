package com.id.microservices.core.product

import com.id.microservices.core.product.persistence.ProductEntity
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.core.env.get
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.index.IndexDefinition
import org.springframework.data.mongodb.core.index.IndexResolver
import org.springframework.data.mongodb.core.index.MongoPersistentEntityIndexResolver

@SpringBootApplication
@ComponentScan("com.id")
class ProductServiceApplication {

    @Bean
    fun afterStartupIndicesInit(mongoTemplate: ReactiveMongoTemplate) =
        ApplicationListener<ContextRefreshedEvent> {
            val resolver: IndexResolver =
                mongoTemplate.converter.mappingContext
                    .let { MongoPersistentEntityIndexResolver(it) }
            val indexOps = mongoTemplate.indexOps(ProductEntity::class.java)
            resolver.resolveIndexFor(ProductEntity::class.java)
                .forEach { e: IndexDefinition -> indexOps.ensureIndex(e).block() }
        }
}

fun main(args: Array<String>) {
    SpringApplication.run(ProductServiceApplication::class.java, *args)
        .also {
            it.environment.let { env ->
                LOG.info("Connected to MongoDb: ${env["spring.data.mongodb.host"]}:${env["spring.data.mongodb.port"]}")
            }
        }
}

private val LOG: Logger = LoggerFactory.getLogger(ProductServiceApplication::class.java)
