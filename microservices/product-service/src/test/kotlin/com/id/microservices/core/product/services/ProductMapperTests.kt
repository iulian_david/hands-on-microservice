package com.id.microservices.core.product.services

import com.id.api.core.product.Product
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.mapstruct.factory.Mappers

class ProductMapperTests {

    private val mapper = Mappers.getMapper(ProductMapper::class.java)
    @Test
    fun mapperTests() {
        assertNotNull(mapper)
        val api = Product(1, "n", 1, "sa")
        val entity = mapper.apiToEntity(api)
        assertThat(api)
            .usingRecursiveComparison()
            .ignoringFields("serviceAddress")
            .isEqualTo(entity)
        val api2 = mapper.entityToApi(entity)
        assertThat(api)
            .usingRecursiveComparison()
            .ignoringFields("serviceAddress")
            .isEqualTo(api2)

        assertNull(api2.serviceAddress)
    }
}
