package com.id.microservices.core.product

import com.id.api.core.product.Product
import com.id.api.event.Event
import com.id.api.exceptions.InvalidInputException
import com.id.microservices.core.product.persistence.ProductRepository
import kotlinx.coroutines.reactor.awaitSingle
import kotlinx.coroutines.reactor.awaitSingleOrNull
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import org.testcontainers.junit.jupiter.Testcontainers
import java.util.function.Consumer

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    properties = [
        "eureka.client.enabled=false",
        "spring.cloud.config.enabled=false",
        "spring.data.mongodb.auto-index-creation=true"
    ]
)
@Testcontainers
class ProductServiceApplicationTests() : MongoDBTestBase() {
    @Autowired private lateinit var client: WebTestClient

    @Autowired
    private lateinit var repository: ProductRepository

    @Autowired
    @Qualifier("messageProcessor")
    private lateinit var messageProcessor: Consumer<Event<Int, Product>>

    @BeforeEach
    fun setupDb() {
        repository.deleteAll().block()
    }

    @Test
    fun getProductById(): Unit = runBlocking {
        val productId = 1
        assertNull(repository.findByProductId(productId).awaitSingleOrNull())
        sendCreateProductEvent(productId)
        assertNotNull(repository.findByProductId(productId).awaitSingle())
        getAndVerifyProduct(productId, HttpStatus.OK)
            .jsonPath("$.productId").isEqualTo(productId)
    }

    @Test
    fun duplicateError(): Unit = runBlocking {
        val productId = 1
        assertNull(repository.findByProductId(productId).awaitSingleOrNull())
        sendCreateProductEvent(productId)
        assertNotNull(repository.findByProductId(productId).awaitSingle())
        assertThrows<InvalidInputException>("Expected a MessagingException here!") {
            sendCreateProductEvent(productId)
        }.also {
            assertEquals("Duplicate key, Product Id: $productId", it.message)
        }
    }

    @Test
    fun deleteProduct(): Unit = runBlocking {
        val productId = 1
        sendCreateProductEvent(productId)
        assertNotNull(repository.findByProductId(productId).awaitSingle())
        sendDeleteProductEvent(productId)
        assertNull(repository.findByProductId(productId).awaitSingleOrNull())
        sendDeleteProductEvent(productId)
    }

    @Test
    fun getProductInvalidParameterString() {
        getAndVerifyProduct("/no-integer", HttpStatus.BAD_REQUEST)
            .jsonPath("$.path").isEqualTo("/product/no-integer")
            .jsonPath("$.message").isEqualTo("Type mismatch.")
    }

    @Test
    fun getProductNotFound() {
        val productIdNotFound = 13
        getAndVerifyProduct(productIdNotFound, HttpStatus.NOT_FOUND)
            .jsonPath("$.path").isEqualTo("/product/$productIdNotFound")
            .jsonPath("$.message").isEqualTo("No product found for productId: $productIdNotFound")
    }

    @Test
    fun getProductInvalidParameterNegativeValue() {
        val productIdInvalid = -1
        getAndVerifyProduct(productIdInvalid, HttpStatus.UNPROCESSABLE_ENTITY)
            .jsonPath("$.path").isEqualTo("/product/$productIdInvalid")
            .jsonPath("$.message").isEqualTo("Invalid productId: $productIdInvalid")
    }

    private fun getAndVerifyProduct(productId: Int, expectedStatus: HttpStatus) =
        getAndVerifyProduct("/$productId", expectedStatus)

    private fun getAndVerifyProduct(productIdPath: String, expectedStatus: HttpStatus) =
        client.get()
            .uri("/product$productIdPath")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isEqualTo(expectedStatus)
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()

    private fun sendCreateProductEvent(productId: Int) {
        val product = Product(productId, "Name $productId", productId, "SA")
        val event = Event(Event.Type.CREATE, productId, product)
        messageProcessor.accept(event)
    }

    private fun sendDeleteProductEvent(productId: Int) {
        val event: Event<Int, Product> = Event(Event.Type.DELETE, productId, null)
        messageProcessor.accept(event)
    }
}
