package com.id.microservices.core.product.persistence

import com.id.microservices.core.product.MongoDBTestBase
import kotlinx.coroutines.reactor.awaitSingle
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.fail
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest
import org.springframework.dao.DuplicateKeyException
import org.springframework.dao.OptimisticLockingFailureException
import org.testcontainers.junit.jupiter.Testcontainers
import reactor.test.StepVerifier

@Testcontainers
@DataMongoTest(
    properties = [
        "spring.cloud.config.enabled=false",
        "spring.data.mongodb.auto-index-creation=true"
    ]
)
internal class ProductRepositoryTests : MongoDBTestBase() {

    companion object {
        private val LOG: Logger = LoggerFactory.getLogger(ProductRepositoryTests::class.java)
    }

    @Autowired
    private lateinit var repository: ProductRepository

    private lateinit var savedEntity: ProductEntity

    private lateinit var id: String

    @BeforeEach
    fun setup(): Unit = runBlocking {
        StepVerifier.create(repository.deleteAll()).verifyComplete()
        val entity = ProductEntity(productId = 1, name = "n", weight = 1)
        savedEntity = repository.save(entity).awaitSingle()

        id = savedEntity.id ?: fail("failed to retrieve id")
        assertThat(savedEntity)
            .usingRecursiveComparison()
            .ignoringFields("id", "version")
            .isEqualTo(entity)

        LOG.info("created entity: $savedEntity")
    }

    @Test
    fun create(): Unit = runBlocking {
        val newEntity = ProductEntity(productId = 2, name = "n", weight = 2)

        val newId = repository.save(newEntity).awaitSingle().id ?: fail("failed to create new product")
        val foundEntity = repository.findById(newId).awaitSingle()
        assertThat(newEntity)
            .usingRecursiveComparison()
            .ignoringFields("id", "version")
            .isEqualTo(foundEntity)
        assertEquals(2, repository.count().awaitSingle())
    }

    @Test
    fun update(): Unit = runBlocking {
        savedEntity.copy(name = "n2")
            .let { repository.save(it) }
            .awaitSingle()

        val foundEntity = repository.findById(id).awaitSingle()
        assertEquals(1, foundEntity.version)
        assertEquals("n2", foundEntity.name)
    }

    @Test
    fun delete() {
        StepVerifier.create(repository.delete(savedEntity)).verifyComplete()
        StepVerifier.create(repository.existsById(id)).expectNext(false).verifyComplete()
    }

    @Test
    fun getByProductId() = runBlocking<Unit> {
        val entity = repository.findByProductId(savedEntity.productId)
            .awaitSingle()
        assertThat(entity)
            .isNotNull
            .usingRecursiveComparison()
            .isEqualTo(savedEntity)
    }

    @Test
    fun duplicateError() = runBlocking<Unit> {
        repository.findByProductId(savedEntity.productId)
            .awaitSingle()
        val entity = ProductEntity(productId = savedEntity.productId, name = "n", weight = 1)
        assertThrows<DuplicateKeyException> {
            repository.save(entity)
                .log()
                .awaitSingle()
        }
    }

    @Test
    fun optimisticLockError() = runBlocking {

        // Store the saved entity in two separate entity objects
        val entity1 = repository.findById(id).awaitSingle()
        val entity2 = repository.findById(id).awaitSingle()

        // Update the entity using the first entity object
        entity1.copy(name = "n1")
            .let { repository.save(it) }
            .awaitSingle()

        //  Update the entity using the second entity object.
        // This should fail since the second entity now holds a old version number, i.e. a Optimistic Lock Error
        try {
            entity2.copy(name = "n1")
                .let { repository.save(it) }
                .awaitSingle()
            fail("Expected an OptimisticLockingFailureException")
        } catch (e: OptimisticLockingFailureException) {}

        // Get the updated entity from the database and verify its new sate
        val updatedEntity = repository.findById(id).awaitSingle()
        assertEquals(1, updatedEntity.version)
        assertEquals("n1", updatedEntity.name)
    }
}
