package com.id.microservices.core.recommendation

import com.id.api.core.recommendation.Recommendation
import com.id.api.event.Event
import com.id.api.exceptions.InvalidInputException
import com.id.microservices.core.recommendation.persistence.RecommendationRepository
import kotlinx.coroutines.reactor.awaitSingle
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.WebTestClient.BodyContentSpec
import org.testcontainers.junit.jupiter.Testcontainers
import reactor.test.StepVerifier
import java.util.function.Consumer

@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    properties = [
        "spring.data.mongodb.auto-index-creation=true"
    ]
)
@Testcontainers
class RecommendationServiceApplicationTests : MongoDBTestBase() {

    @Autowired
    private lateinit var client: WebTestClient

    @Autowired
    private lateinit var repository: RecommendationRepository

    @Autowired
    @Qualifier("messageProcessor")
    private lateinit var messageProcessor: Consumer<Event<Int, Recommendation>>

    @BeforeEach
    fun setupDb() {
        StepVerifier.create(repository.deleteAll()).verifyComplete()
    }

    @Test
    fun getRecommendationsByProductId() {
        val productId = 1

        sendCreateRecommendationEvent(productId, 1)
        sendCreateRecommendationEvent(productId, 2)
        sendCreateRecommendationEvent(productId, 3)
        assertThat(repository.findByProductId(productId).collectList().block()).hasSize(3)
        getAndVerifyRecommendationsByProductId(productId, HttpStatus.OK)
            .also {
                println(it.returnResult().responseBody?.let { it1 -> String(it1) })
            }
            .jsonPath("$.length()").isEqualTo(3)
            .jsonPath("$[2].productId").isEqualTo(productId)
            .jsonPath("$[2].recommendationId").isEqualTo(3)
    }

    @Test
    fun duplicateError() = runBlocking {
        val productId = 1
        val recommendationId = 1

        sendCreateRecommendationEvent(productId, recommendationId)

        assertEquals(1, repository.count().awaitSingle())

        assertThrows<InvalidInputException>("Expected a InvalidInputException here!") {
            sendCreateRecommendationEvent(productId, recommendationId)
        }.also {
            assertEquals("Duplicate key, Product Id: 1, Recommendation Id:1", it.message)
        }

        assertEquals(1, repository.count().awaitSingle())
    }

    @Test
    fun deleteRecommendations() = runBlocking {
        val productId = 1
        val recommendationId = 1

        sendCreateRecommendationEvent(productId, recommendationId)
        assertThat(repository.findByProductId(productId).collectList().awaitSingle()).hasSize(1)

        sendDeleteRecommendationEvent(productId)
        assertThat(repository.findByProductId(productId).collectList().awaitSingle()).hasSize(0)

        sendDeleteRecommendationEvent(productId)
    }

    @Test
    fun getRecommendationsMissingParameter() {
        getAndVerifyRecommendationsByProductId("", HttpStatus.BAD_REQUEST)
            .jsonPath("$.path").isEqualTo("/recommendation")
            .jsonPath("$.message").isEqualTo("Required query parameter 'productId' is not present.")
    }

    @Test
    fun getRecommendationsInvalidParameter() {
        getAndVerifyRecommendationsByProductId("?productId=no-integer", HttpStatus.BAD_REQUEST)
            .jsonPath("$.path").isEqualTo("/recommendation")
            .jsonPath("$.message").isEqualTo("Type mismatch.")
    }

    @Test
    fun getRecommendationsNotFound() {
        getAndVerifyRecommendationsByProductId("?productId=113", HttpStatus.OK)
            .jsonPath("$.length()").isEqualTo(0)
    }

    @Test
    fun getRecommendationsInvalidParameterNegativeValue() {
        val productIdInvalid = -1
        getAndVerifyRecommendationsByProductId("?productId=$productIdInvalid", HttpStatus.UNPROCESSABLE_ENTITY)
            .jsonPath("$.path").isEqualTo("/recommendation")
            .jsonPath("$.message").isEqualTo("Invalid productId: $productIdInvalid")
    }

    private fun getAndVerifyRecommendationsByProductId(
        productId: Int,
        expectedStatus: HttpStatus
    ): BodyContentSpec {
        return getAndVerifyRecommendationsByProductId("?productId=$productId", expectedStatus)
    }

    private fun getAndVerifyRecommendationsByProductId(
        productIdQuery: String,
        expectedStatus: HttpStatus
    ): BodyContentSpec {
        return client.get()
            .uri("/recommendation$productIdQuery")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isEqualTo(expectedStatus)
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
    }
    private fun sendCreateRecommendationEvent(productId: Int, recommendationId: Int) {
        val recommendation = Recommendation(
            productId, recommendationId,
            "Author $recommendationId", recommendationId, "Content $recommendationId", "SA"
        )
        val event = Event(Event.Type.CREATE, productId, recommendation)
        messageProcessor.accept(event)
    }

    private fun sendDeleteRecommendationEvent(productId: Int) {
        val event: Event<Int, Recommendation> = Event(Event.Type.DELETE, productId, null)
        messageProcessor.accept(event)
    }
}
