package com.id.microservices.core.recommendation
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.MongoDBContainer

abstract class MongoDBTestBase {

    companion object {
        private val database = MongoDBContainer("mongo:latest")
        private val LOG: Logger = LoggerFactory.getLogger(MongoDBTestBase::class.java)

        @DynamicPropertySource
        @JvmStatic
        fun databaseProperties(registry: DynamicPropertyRegistry) {
            LOG.error("replicaSetUrl: ${database.replicaSetUrl}")
            registry.add("spring.data.mongodb.uri") { database.replicaSetUrl }
        }
    }

    init {
        database.start()
    }
}
