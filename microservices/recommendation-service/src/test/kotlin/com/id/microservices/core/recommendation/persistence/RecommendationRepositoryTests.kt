package com.id.microservices.core.recommendation.persistence

import com.id.microservices.core.recommendation.MongoDBTestBase
import kotlinx.coroutines.reactor.awaitSingle
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest
import org.springframework.dao.DuplicateKeyException
import org.springframework.dao.OptimisticLockingFailureException
import org.testcontainers.junit.jupiter.Testcontainers
import reactor.test.StepVerifier

@Testcontainers
@DataMongoTest(
    properties = [
        "spring.data.mongodb.auto-index-creation=true"
    ]
)
internal class RecommendationRepositoryTests : MongoDBTestBase() {

    companion object {
        private val LOG: Logger = LoggerFactory.getLogger(RecommendationRepositoryTests::class.java)
    }

    @Autowired
    private lateinit var repository: RecommendationRepository

    private lateinit var savedEntity: RecommendationEntity

    @BeforeEach
    fun setup() = runBlocking<Unit> {
        StepVerifier.create(repository.deleteAll()).verifyComplete()
        val entity = RecommendationEntity(productId = 1, recommendationId = 2, author = "a", rating = 3, content = "c")
        savedEntity = repository.save(entity).awaitSingle()
        LOG.info("created entity: $savedEntity")
        assertThat(savedEntity)
            .usingRecursiveComparison()
            .ignoringFields("id", "version")
            .isEqualTo(entity)
    }

    @Test
    fun create() = runBlocking {
        val newEntity = RecommendationEntity(
            productId = 1,
            recommendationId = 3,
            author = "a",
            rating = 3,
            content = "c"
        ).let {
            repository.save(it)
        }.awaitSingle()
        val foundEntity = repository.findById(newEntity.id!!).awaitSingle()
        assertThat(newEntity)
            .usingRecursiveComparison()
            .ignoringFields("version")
            .isEqualTo(foundEntity)
        assertEquals(2, repository.count().awaitSingle())
    }

    @Test
    fun update() = runBlocking {
        savedEntity.copy(author = "a2")
            .let { repository.save(it) }
            .awaitSingle()

        val foundEntity = repository.findById(savedEntity.id!!).awaitSingle()
        assertEquals(1, foundEntity.version)
        assertEquals("a2", foundEntity.author)
    }

    @Test
    fun delete() = runBlocking<Unit> {
        StepVerifier.create(repository.delete(savedEntity)).verifyComplete()
        assertFalse(repository.existsById(savedEntity.id!!).awaitSingle())
    }

    @Test
    fun getByProductId() = runBlocking<Unit> {
        val entityList = repository.findByProductId(savedEntity.productId).collectList().awaitSingle()
        assertThat(entityList).hasSize(1)
        assertThat(entityList[0])
            .usingRecursiveComparison()
            .ignoringFields("id")
            .isEqualTo(savedEntity)
    }

    @Test
    fun duplicateError() = runBlocking<Unit> {
        val entity = RecommendationEntity(
            productId = 1,
            recommendationId = 2,
            author = "a",
            rating = 3,
            content = "c"
        )
        assertThrows<DuplicateKeyException> {
            repository.save(entity)
                .log()
                .awaitSingle()
        }
    }

    @Test
    fun optimisticLockError() = runBlocking<Unit> {

        // Store the saved entity in two separate entity objects
        val entity1 = repository.findById(savedEntity.id!!).awaitSingle()
        val entity2 = repository.findById(savedEntity.id!!).awaitSingle()

        // Update the entity using the first entity object
        entity1.copy(author = "a1")
            .let {
                repository.save(it)
            }.awaitSingle()

        //  Update the entity using the second entity object.
        // This should fail since the second entity now holds a old version number, i.e. a Optimistic Lock Error
        try {
            entity2.copy(author = "a2")
                .let { repository.save(it) }
                .awaitSingle()
            Assertions.fail("Expected an OptimisticLockingFailureException")
        } catch (e: OptimisticLockingFailureException) {
        }

        // Get the updated entity from the database and verify its new sate
        val updatedEntity = repository.findById(savedEntity.id!!).awaitSingle()
        assertEquals(1, updatedEntity.version)
        assertEquals("a1", updatedEntity.author)
    }
}
