package com.id.microservices.core.recommendation.service

import com.id.api.core.recommendation.Recommendation
import com.id.microservices.core.recommendation.persistence.RecommendationEntity
import com.id.microservices.core.recommendation.services.RecommendationMapper
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.mapstruct.factory.Mappers

class RecommendationMapperTests {

    private val mapper: RecommendationMapper = Mappers.getMapper(RecommendationMapper::class.java)

    @Test
    fun mapperTests() {
        assertNotNull(mapper)
        val api = Recommendation(1, 2, "a", 4, "C", "adr")
        val entity: RecommendationEntity = mapper.apiToEntity(api)
        assertThat(api)
            .usingRecursiveComparison()
            .ignoringFields("id", "version", "serviceAddress", "rate")
            .isEqualTo(entity)
        assertEquals(api.rate, entity.rating)

        val api2: Recommendation = mapper.entityToApi(entity)
        assertThat(api)
            .usingRecursiveComparison()
            .ignoringFields("id", "version", "serviceAddress")
            .isEqualTo(api2)
        assertNull(api2.serviceAddress)
    }

    @Test
    fun mapperListTests() {
        assertNotNull(mapper)
        val api = Recommendation(1, 2, "a", 4, "C", "adr")
        val apiList: List<Recommendation> = listOf(api)
        val entityList: List<RecommendationEntity> = mapper.apiListToEntityList(apiList)
        assertThat(apiList)
            .usingRecursiveComparison()
            .ignoringFields("id", "version", "serviceAddress", "rate")
            .isEqualTo(entityList)
        assertThat(apiList.map { it.rate })
            .containsExactlyInAnyOrderElementsOf(entityList.map { it.rating })

        val api2List: List<Recommendation> = mapper.entityListToApiList(entityList)
        assertThat(apiList)
            .usingRecursiveComparison()
            .ignoringFields("id", "version", "serviceAddress")
            .isEqualTo(api2List)
        assertNull(api2List[0].serviceAddress)
    }
}
