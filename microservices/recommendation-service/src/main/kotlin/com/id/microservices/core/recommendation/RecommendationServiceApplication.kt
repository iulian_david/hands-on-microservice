package com.id.microservices.core.recommendation

import com.id.microservices.core.recommendation.persistence.RecommendationEntity
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.index.IndexDefinition
import org.springframework.data.mongodb.core.index.IndexResolver
import org.springframework.data.mongodb.core.index.MongoPersistentEntityIndexResolver

@SpringBootApplication
@ComponentScan("com.id")
class RecommendationServiceApplication {

    @Bean
    fun afterStartupIndicesInit(mongoTemplate: ReactiveMongoTemplate) =
        ApplicationListener<ContextRefreshedEvent> {
            val mappingContext = mongoTemplate.converter.mappingContext
            val resolver: IndexResolver = MongoPersistentEntityIndexResolver(mappingContext)
            val indexOps = mongoTemplate.indexOps(RecommendationEntity::class.java)
            resolver.resolveIndexFor(RecommendationEntity::class.java)
                .forEach { e: IndexDefinition -> indexOps.ensureIndex(e).block() }
        }
}

fun main(args: Array<String>) {
    SpringApplication.run(RecommendationServiceApplication::class.java, *args)
        .also {
            LOG.info(
                """
               Connected to MongoDb: ${it.environment.getProperty("spring.data.mongodb.host")}:
        ${it.environment.getProperty("spring.data.mongodb.port")}
                """.trimIndent()
            )
        }
}

private val LOG: Logger = LoggerFactory.getLogger(RecommendationServiceApplication::class.java)
