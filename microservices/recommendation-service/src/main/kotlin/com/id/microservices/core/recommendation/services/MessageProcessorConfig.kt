package com.id.microservices.core.recommendation.services

import com.id.api.core.recommendation.Recommendation
import com.id.api.core.recommendation.RecommendationService
import com.id.api.event.Event
import com.id.api.exceptions.EventProcessingException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.function.Consumer

@Configuration
class MessageProcessorConfig(
    private val recommendationService: RecommendationService
) {
    companion object {
        private val LOG: Logger =
            LoggerFactory.getLogger(MessageProcessorConfig::class.java)
    }

    @Bean
    fun messageProcessor(): Consumer<Event<Int, Recommendation>> {
        return Consumer { event ->
            LOG.info("Process message created at {}...", event.eventCreatedAt)
            when (event.eventType) {
                Event.Type.CREATE -> {
                    val recommendation = event.data ?: error("Invalid input")
                    LOG.info(
                        "Create recommendation with ID: {}/{}",
                        recommendation.productId,
                        recommendation.recommendationId
                    )
                    recommendationService.createRecommendation(recommendation).block()
                }
                Event.Type.DELETE -> {
                    val productId: Int = event.key ?: error("Invalid id")
                    LOG.info("Delete recommendations with ProductID: {}", productId)
                    recommendationService.deleteRecommendations(productId).block()
                }
                else -> {
                    val errorMessage = "Incorrect event type: ${event.eventType}, expected a CREATE or DELETE event"
                    LOG.warn(errorMessage)
                    throw EventProcessingException(errorMessage)
                }
            }
            LOG.info("Message processing done!")
        }
    }
}
