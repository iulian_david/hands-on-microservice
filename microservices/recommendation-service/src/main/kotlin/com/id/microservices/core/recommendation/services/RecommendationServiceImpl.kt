package com.id.microservices.core.recommendation.services

import com.id.api.core.recommendation.Recommendation
import com.id.api.core.recommendation.RecommendationService
import com.id.api.exceptions.InvalidInputException
import com.id.microservices.core.recommendation.persistence.RecommendationRepository
import com.id.util.http.ServiceUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DuplicateKeyException
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.logging.Level.FINE

@RestController
class RecommendationServiceImpl(
    @Autowired private val serviceUtil: ServiceUtil,
    @Autowired private val mapper: RecommendationMapper,
    @Autowired private val repository: RecommendationRepository
) : RecommendationService {

    companion object {
        private val LOG: Logger = LoggerFactory.getLogger(RecommendationServiceImpl::class.java)
    }

    override fun createRecommendation(body: Recommendation): Mono<Recommendation> =
        when {
            body.productId < 1 -> throw InvalidInputException("Invalid productId: ${body.productId}")
            else -> {
                val entity = mapper.apiToEntity(body)
                repository.save(entity)
                    .log()
                    .onErrorMap(DuplicateKeyException::class.java) {
                        InvalidInputException(
                            "Duplicate key, Product Id: ${body.productId}, Recommendation Id:${body.recommendationId}"
                        )
                    }
                    .map { e -> mapper.entityToApi(e) }
            }
        }

    override fun getRecommendations(headers: HttpHeaders, productId: Int): Flux<Recommendation> {
        return when {
            productId < 1 -> {
                throw InvalidInputException("Invalid productId: $productId")
            }
            else -> {
                repository.findByProductId(productId)
                    .log(LOG.name, FINE)
                    .map(mapper::entityToApi)
                    .map {
                        it.copy(
                            serviceAddress = serviceUtil.serviceAddress ?: "Unknown address"
                        )
                    }
            }
        }
    }

    override fun deleteRecommendations(productId: Int): Mono<Void> =
        when {
            productId < 1 -> throw InvalidInputException("Invalid productId: $productId")
            else -> {
                LOG.debug(
                    "deleteRecommendations: tries to delete recommendations for the product with productId: {}",
                    productId
                )
                repository.deleteAll(repository.findByProductId(productId))
            }
        }
}
