import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "3.0.5"
    id("io.spring.dependency-management") version "1.0.13.RELEASE"
    kotlin("jvm") version "1.8.10"
    kotlin("plugin.spring") version "1.8.10"
    kotlin("kapt") version "1.8.10"
    id("org.jmailen.kotlinter") version "3.6.0"
}


group = "com.id"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17

val springCloudVersion = "2022.0.1"
val resilience4jVersion = "2.0.2"

repositories {
	mavenCentral()
}

dependencies {
	implementation(project(":api"))
	implementation(project(":util"))
	implementation("org.springframework.boot:spring-boot-starter-actuator")
	implementation("org.springframework.boot:spring-boot-starter-webflux")
	implementation("org.springframework.boot:spring-boot-starter-oauth2-resource-server")
	implementation("org.springframework.cloud:spring-cloud-starter-stream-rabbit")
	implementation("org.springframework.cloud:spring-cloud-starter-stream-kafka")
	implementation("org.springframework.cloud:spring-cloud-commons")
	implementation("io.micrometer:micrometer-tracing-bridge-otel")
	implementation("io.micrometer:micrometer-registry-prometheus")
	implementation("org.springframework.retry:spring-retry")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
	implementation("org.springdoc:springdoc-openapi-starter-webflux-ui:2.1.0")
	implementation("org.springdoc:springdoc-openapi-starter-common:2.1.0")
	implementation("io.github.resilience4j:resilience4j-spring-boot3:${resilience4jVersion}")
	implementation("io.github.resilience4j:resilience4j-reactor:${resilience4jVersion}")
	implementation("org.springframework.boot:spring-boot-starter-aop")

	testImplementation("org.springframework.boot:spring-boot-starter-test") {
		exclude(group = "org.junit.vintage", module= "junit-vintage-engine")
		exclude(module = "mockito-core")
	}
	testImplementation("org.springframework.cloud:spring-cloud-stream-test-binder")

	testImplementation("io.projectreactor:reactor-test")
	testImplementation("com.ninja-squad:springmockk:3.0.1")
	testImplementation("io.projectreactor:reactor-test")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "17"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}


tasks.getByName<Jar>("jar") {
	enabled = false
}

//kapt {
//	arguments {
//		// Set Mapstruct Configuration options here
//		// https://kotlinlang.org/docs/reference/kapt.html#annotation-processor-arguments
//		// https://mapstruct.org/documentation/stable/reference/html/#configuration-options
//		arg("mapstruct.defaultComponentModel", "spring")
//	}
//}

dependencyManagement {
	imports {
		mavenBom("org.springframework.cloud:spring-cloud-dependencies:${springCloudVersion}")
	}

	dependencies {
		dependency("io.github.resilience4j:resilience4j-spring:${resilience4jVersion}")
		dependency("io.github.resilience4j:resilience4j-annotations:${resilience4jVersion}")
		dependency("io.github.resilience4j:resilience4j-consumer:${resilience4jVersion}")
		dependency("io.github.resilience4j:resilience4j-core:${resilience4jVersion}")
		dependency("io.github.resilience4j:resilience4j-circuitbreaker:${resilience4jVersion}")
		dependency("io.github.resilience4j:resilience4j-ratelimiter:${resilience4jVersion}")
		dependency("io.github.resilience4j:resilience4j-retry:${resilience4jVersion}")
		dependency("io.github.resilience4j:resilience4j-bulkhead:${resilience4jVersion}")
		dependency("io.github.resilience4j:resilience4j-timelimiter:${resilience4jVersion}")
		dependency("io.github.resilience4j:resilience4j-micrometer:${resilience4jVersion}")
		dependency("io.github.resilience4j:resilience4j-circularbuffer:${resilience4jVersion}")
	}
}