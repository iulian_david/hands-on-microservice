package com.id.microservices.composite.product

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.loadbalancer.LoadBalanced
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.scheduler.Scheduler
import reactor.core.scheduler.Schedulers

@SpringBootApplication
@ComponentScan("com.id")
class ProductCompositeServiceApplication(
    @Autowired @Value("\${app.threadPoolSize:10}") val threadPoolSize: Int,
    @Autowired @Value("\${app.taskQueueSize:100}") val taskQueueSize: Int
) {

    companion object {
        private val LOG = LoggerFactory.getLogger(ProductCompositeServiceApplication::class.java)
    }

    @Bean
    fun publishEventScheduler(): Scheduler {
        LOG.info(
            "Creates a messagingScheduler with connectionPoolSize = {}",
            threadPoolSize
        )
        return Schedulers.newBoundedElastic(threadPoolSize, taskQueueSize, "publish-pool")
    }

    @Bean
    @LoadBalanced
    fun loadBalancedWebClientBuilder() = WebClient.builder()
}

fun main(args: Array<String>) {
    runApplication<ProductCompositeServiceApplication>(*args)
}
