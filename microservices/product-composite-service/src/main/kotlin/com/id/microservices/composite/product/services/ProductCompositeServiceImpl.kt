package com.id.microservices.composite.product.services

import com.id.api.composite.product.ProductAggregate
import com.id.api.composite.product.ProductCompositeService
import com.id.api.composite.product.RecommendationSummary
import com.id.api.composite.product.ReviewSummary
import com.id.api.composite.product.ServiceAddresses
import com.id.api.core.product.Product
import com.id.api.core.recommendation.Recommendation
import com.id.api.core.review.Review
import com.id.util.http.ServiceUtil
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.security.core.context.ReactiveSecurityContextHolder
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextImpl
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import reactor.util.function.Tuple2
import reactor.util.function.Tuple3
import reactor.util.function.Tuple4
import reactor.util.function.Tuple5

@RestController
class ProductCompositeServiceImpl(
    @Autowired private val serviceUtil: ServiceUtil,
    @Autowired private val integration: ProductCompositeIntegration
) : ProductCompositeService {

    private val nullSecCtx: SecurityContext = SecurityContextImpl()

    companion object {
        private val LOG = LoggerFactory.getLogger(ProductCompositeService::class.java)
    }

    override fun createProduct(body: ProductAggregate): Mono<Void> {
        try {
            val monoList = arrayListOf<Mono<*>>()
            monoList.add(getLogAuthorizationInfo())
            LOG.debug(
                "createCompositeProduct: creates a new composite entity for productId: {}",
                body.productId
            )
            val product = Product(body.productId, body.name, body.weight, null)
            monoList.add(integration.createProduct(product))
            body.recommendations
                ?.map {
                    Recommendation(
                        body.productId,
                        it.recommendationId,
                        it.author,
                        it.rate,
                        it.content
                    )
                }
                ?.forEach {
                    monoList.add(integration.createRecommendation(it))
                }

            body.reviews
                ?.map {
                    Review(
                        body.productId,
                        it.reviewId,
                        it.author,
                        it.subject,
                        it.content
                    )
                }
                ?.forEach { monoList.add(integration.createReview(it)) }

            LOG.debug(
                "createCompositeProduct: composite entites created for productId: {}",
                body.productId
            )

            return Mono.zip({ "" }, *monoList.toTypedArray())
                .doOnError { ex: Throwable ->
                    LOG.warn(
                        "createCompositeProduct failed: {}",
                        ex.toString()
                    )
                }.then()
        } catch (re: RuntimeException) {
            LOG.warn("createCompositeProduct failed", re)
            throw re
        }
    }

    override fun getProduct(
        headers: HttpHeaders,
        productId: Int,
        delay: Int?,
        faultPercent: Int?
    ): Mono<ProductAggregate> {
        val requestHeaders: HttpHeaders = getHeaders(headers, "X-group")
        return Mono.zip(
            ReactiveSecurityContextHolder.getContext().defaultIfEmpty(nullSecCtx),
            integration.getProduct(requestHeaders, productId, delay ?: 0, faultPercent ?: 0),
            integration.getRecommendations(requestHeaders, productId).collectList(),
            integration.getReviews(requestHeaders, productId).collectList(),
            Mono.just(serviceUtil.serviceAddress ?: "Unknown address")
        )
            .map { (sc, product, recommendations, reviews, serviceAddress) ->
                createProductAggregate(
                    sc,
                    product,
                    recommendations,
                    reviews,
                    serviceAddress
                )
            }
            .doOnError { ex: Throwable ->
                LOG.warn("getCompositeProduct failed: {}", ex.toString())
            }
            .log()
    }

    override fun deleteProduct(productId: Int): Mono<Void> =
        try {
            LOG.debug("deleteCompositeProduct: Deletes a product aggregate for productId: {}", productId)
            Mono.zip(
                { "" },
                getLogAuthorizationInfo(),
                integration.deleteProduct(productId),
                integration.deleteRecommendations(productId),
                integration.deleteReviews(productId)
            )
                .doOnError { LOG.warn("delete failed: {}", it.toString()) }
                .log()
                .then()
        } catch (re: RuntimeException) {
            LOG.warn("deleteCompositeProduct failed: {}", re.toString())
            throw re
        }

    private fun createProductAggregate(
        sc: SecurityContext,
        product: Product,
        recommendations: List<Recommendation>,
        reviews: List<Review>,
        serviceAddress: String
    ): ProductAggregate {
        logAuthorizationInfo(sc)
        // 1. Setup product info
        val productId: Int = product.productId
        val name: String = product.name
        val weight: Int = product.weight

        // 2. Copy summary recommendation info, if available
        val recommendationSummaries = recommendations
            .map { r -> RecommendationSummary(r.recommendationId, r.author ?: "Unknown author", r.rate, r.content) }

        // 3. Copy summary review info, if available
        val reviewSummaries = reviews
            .map { r -> ReviewSummary(r.reviewId, r.author ?: "Unknown author", r.subject ?: "No subject", r.content ?: "No content") }

        // 4. Create info regarding the involved microservices addresses
        val productAddress: String = product.serviceAddress ?: "Unknown address"

        val reviewAddress = if (reviews.isEmpty()) "" else reviews[0].serviceAddress
        val recommendationAddress = if (recommendations.isEmpty()) "" else recommendations[0].serviceAddress
        val serviceAddresses = ServiceAddresses(serviceAddress, productAddress, reviewAddress, recommendationAddress)

        return ProductAggregate(productId, name, weight, recommendationSummaries, reviewSummaries, serviceAddresses)
    }

    private fun getHeaders(requestHeaders: HttpHeaders, vararg headers: String): HttpHeaders {
        LOG.trace("Will look for {} headers: {}", headers.size, headers)
        val h = HttpHeaders()
        for (header in headers) {
            val value = requestHeaders[header]
            if (value != null) {
                h.addAll(header, value)
            }
        }

        LOG.trace("Will transfer {}, headers: {}", h.size, h)
        return h
    }

    private fun getLogAuthorizationInfo(): Mono<SecurityContext> =
        ReactiveSecurityContextHolder.getContext().defaultIfEmpty(nullSecCtx)
            .doOnNext { sc ->
                logAuthorizationInfo(sc)
            }

    private fun logAuthorizationInfo(sc: SecurityContext?) =
        if (sc?.authentication is JwtAuthenticationToken) {
            val jwtToken = (sc.authentication as JwtAuthenticationToken).token
            logAuthorizationInfoJwt(jwtToken)
        } else {
            LOG.warn("No JWT based Authentication supplied, running tests are?")
        }

    private fun logAuthorizationInfoJwt(jwt: Jwt?) =
        if (jwt == null) {
            LOG.warn("No JWT supplied, running tests are?")
        } else {
            if (LOG.isDebugEnabled) {
                LOG.debug(
                    "Authorization info: Subject: {}, scopes: {}, expires: {}, issuer: {}, audience: {}",
                    jwt.issuer, jwt.audience, jwt.claims["sub"], jwt.claims["scope"], jwt.claims["exp"]
                )
            } else {}
        }
}

private operator fun <T1, T2, T3, T4, T5> Tuple5<T1, T2, T3, T4, T5>.component5(): T5 = this.t5
private operator fun <T1, T2, T3, T4> Tuple4<T1, T2, T3, T4>.component4(): T4 = this.t4

private operator fun <T1, T2, T3> Tuple3<T1, T2, T3>.component3(): T3 = this.t3

private operator fun <T1, T2> Tuple2<T1, T2>.component1(): T1 = this.t1

private operator fun <T1, T2> Tuple2<T1, T2>.component2(): T2 = this.t2
