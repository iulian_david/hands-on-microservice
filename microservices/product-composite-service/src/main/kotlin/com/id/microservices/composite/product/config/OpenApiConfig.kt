package com.id.microservices.composite.product.config

import io.swagger.v3.oas.annotations.enums.SecuritySchemeType
import io.swagger.v3.oas.annotations.security.OAuthFlow
import io.swagger.v3.oas.annotations.security.OAuthFlows
import io.swagger.v3.oas.annotations.security.OAuthScope
import io.swagger.v3.oas.annotations.security.SecurityScheme
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Contact
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.info.License
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@SecurityScheme(
    name = "security_auth",
    type = SecuritySchemeType.OAUTH2,
    flows = OAuthFlows(
        authorizationCode = OAuthFlow(
            authorizationUrl = "\${springdoc.oAuthFlow.authorizationUrl}",
            tokenUrl = "\${springdoc.oAuthFlow.tokenUrl}",
            scopes = [
                OAuthScope(name = "product:read", description = "read scope"), OAuthScope(
                    name = "product:write",
                    description = "write scope"
                )
            ]
        )
    )
)
class OpenApiConfig(
    @Value("\${api.common.version:no-version}") val apiVersion: String,
    @Value("\${api.common.title:no-title}") val apiTitle: String,
    @Value("\${api.common.description:no-description}") val apiDescription: String,
    @Value("\${api.common.termsOfServiceUrl:incognito}") val apiTermsOfServiceUrl: String,
    @Value("\${api.common.license:mit}") val apiLicense: String,
    @Value("\${api.common.licenseUrl:noUrl}") val apiLicenseUrl: String,
    @Value("\${api.common.contact.name:noone}") val apiContactName: String,
    @Value("\${api.common.contact.url:you-wish}") val apiContactUrl: String,
    @Value("\${api.common.contact.email:none}") val apiContactEmail: String,
    @Value("\${api.common.terms:no-terms}") val apiTermsOfService: String
) {

    /**
     * Will exposed on $HOST:$PORT/swagger-ui.html
     *
     * @return
     */
    @Bean
    fun getOpenApiDoc(): OpenAPI =
        OpenAPI()
            .info(
                Info().title(apiTitle)
                    .description(apiDescription)
                    .version(apiVersion)
                    .contact(
                        Contact().name(apiContactName)
                            .email(apiContactEmail)
                            .url(apiContactUrl)
                    )
                    .termsOfService(apiTermsOfService)
                    .license(
                        License()
                            .name(apiLicense)
                            .url(apiLicenseUrl)
                    )
            )
}
