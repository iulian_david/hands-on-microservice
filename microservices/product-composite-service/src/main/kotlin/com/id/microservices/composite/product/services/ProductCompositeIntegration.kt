package com.id.microservices.composite.product.services

import com.fasterxml.jackson.databind.ObjectMapper
import com.id.api.core.product.Product
import com.id.api.core.product.ProductService
import com.id.api.core.recommendation.Recommendation
import com.id.api.core.recommendation.RecommendationService
import com.id.api.core.review.Review
import com.id.api.core.review.ReviewService
import com.id.api.event.Event
import com.id.api.exceptions.InvalidInputException
import com.id.api.exceptions.NotFoundException
import com.id.util.http.HttpErrorInfo
import com.id.util.http.ServiceUtil
import io.github.resilience4j.circuitbreaker.CallNotPermittedException
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker
import io.github.resilience4j.retry.annotation.Retry
import io.github.resilience4j.timelimiter.annotation.TimeLimiter
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.cloud.stream.function.StreamBridge
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.messaging.support.MessageBuilder
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.WebClientResponseException
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.scheduler.Scheduler
import java.io.IOException
import java.util.logging.Level

@Component
class ProductCompositeIntegration(
    @Autowired private val webClientBuilder: WebClient.Builder,
    @Autowired val mapper: ObjectMapper,
    @Autowired val streamBridge: StreamBridge,
    @Autowired @Qualifier("publishEventScheduler") val publishEventScheduler: Scheduler,
    @Autowired val serviceUtil: ServiceUtil
) : ProductService, ReviewService, RecommendationService {

    private val webClient: WebClient = webClientBuilder.build()

    companion object {
        private const val productServiceUrl: String = "http://product"
        private const val recommendationServiceUrl: String = "http://recommendation"
        private const val reviewServiceUrl: String = "http://review"
        private val LOG: Logger = LoggerFactory.getLogger(ProductCompositeIntegration::class.java)
    }

    override fun createProduct(body: Product): Mono<Product> =
        Mono.fromCallable {
            sendMessage("products-out-0", Event(Event.Type.CREATE, body.productId, body))
            body
        }.subscribeOn(publishEventScheduler)

    @Retry(name = "product")
    @TimeLimiter(name = "product")
    @CircuitBreaker(name = "product", fallbackMethod = "getFallbackValue")
    override fun getProduct(
        headers: HttpHeaders,
        productId: Int,
        delay: Int,
        faultPercent: Int
    ): Mono<Product> {
        LOG.debug("Will call getProduct API on URL: $productServiceUrl$productId")
        return webClient.get()
            .uri("$productServiceUrl/product/$productId?delay=$delay&faultPercent=$faultPercent")
            .headers { it.addAll(headers) }
            .retrieve()
            .bodyToMono(Product::class.java)
            .log(LOG.name, Level.FINE)
            .onErrorMap(WebClientResponseException::class.java) {
                handleException(it)
            }
    }

    fun getFallbackValue(
        headers: HttpHeaders,
        productId: Int,
        delay: Int,
        faultPercent: Int,
        ex: CallNotPermittedException
    ): Mono<Product> {
        LOG.warn(
            "Creating a fail-fast fallback product for productId = {}, delay = {}, faultPercent = {} and exception = {} ",
            productId, delay, faultPercent, ex.toString()
        )
        if (productId < 1) {
            throw InvalidInputException("Invalid productId: $productId")
        }
        if (productId == 13) {
            val errMsg = "Product Id: $productId not found in fallback cache!"
            LOG.warn(errMsg)
            throw NotFoundException(errMsg)
        }

        return Mono.just(Product(productId, "Fallback product$productId", productId, serviceUtil.serviceAddress))
    }

    override fun deleteProduct(productId: Int) =
        Mono.fromCallable {
            sendMessage("products-out-0", Event(Event.Type.DELETE, productId, null))
        }.subscribeOn(publishEventScheduler).then()

    override fun createRecommendation(body: Recommendation): Mono<Recommendation> =
        Mono.fromCallable {
            sendMessage("recommendations-out-0", Event(Event.Type.CREATE, body.productId, body))
            body
        }.subscribeOn(publishEventScheduler)

    override fun getRecommendations(headers: HttpHeaders, productId: Int): Flux<Recommendation> =
        webClient
            .get()
            .uri("$recommendationServiceUrl/recommendation?productId=$productId")
            .headers { it.addAll(headers) }
            .retrieve()
            .bodyToFlux(Recommendation::class.java)
            .log()
            .onErrorResume { Flux.empty() }

    override fun deleteRecommendations(productId: Int) =
        Mono.fromCallable {
            sendMessage("recommendations-out-0", Event(Event.Type.DELETE, productId, null))
        }.subscribeOn(publishEventScheduler).then()

    override fun createReview(body: Review): Mono<Review> =
        Mono.fromCallable {
            sendMessage("reviews-out-0", Event(Event.Type.CREATE, body.productId, body))
            body
        }.subscribeOn(publishEventScheduler)

    override fun getReviews(headers: HttpHeaders, productId: Int): Flux<Review> =
        webClient
            .get()
            .uri("$reviewServiceUrl/review?productId=$productId")
            .headers { it.addAll(headers) }
            .retrieve()
            .bodyToFlux(Review::class.java)
            .log()
            .onErrorResume { Flux.empty() }

    override fun deleteReviews(productId: Int) =
        Mono.fromCallable {
            sendMessage("reviews-out-0", Event(Event.Type.DELETE, productId, null))
        }.subscribeOn(publishEventScheduler).then()

    private fun getErrorMessage(ex: WebClientResponseException): String {
        return try {
            mapper.readValue(ex.responseBodyAsString, HttpErrorInfo::class.java).message
                ?: "No error"
        } catch (ioex: IOException) {
            ex.message ?: "Unknown exception"
        }
    }

    private fun handleException(ex: Throwable): Throwable {
        if (ex !is WebClientResponseException) {
            LOG.warn("Got a unexpected error: {}, will rethrow it", ex.toString())
            return ex
        }
        val wcre = ex
        return when (wcre.statusCode) {
            HttpStatus.NOT_FOUND -> NotFoundException(getErrorMessage(wcre))
            HttpStatus.UNPROCESSABLE_ENTITY -> InvalidInputException(getErrorMessage(wcre))
            else -> {
                LOG.warn(
                    "Got a unexpected HTTP error: {}, will rethrow it",
                    wcre.statusCode
                )
                LOG.warn("Error body: {}", wcre.responseBodyAsString)
                ex
            }
        }
    }

    private fun sendMessage(bindingName: String, event: Event<*, *>) {
        LOG.debug("Sending a {} message to {}", event.eventType, bindingName)
        val message = MessageBuilder.withPayload(event)
            .setHeader("partitionKey", event.key)
            .build()
        streamBridge.send(bindingName, message)
    }
}
