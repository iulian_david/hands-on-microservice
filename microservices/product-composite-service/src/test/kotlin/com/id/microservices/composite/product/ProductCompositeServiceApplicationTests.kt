package com.id.microservices.composite.product

import com.id.api.composite.product.ProductAggregate
import com.id.api.core.product.Product
import com.id.api.core.recommendation.Recommendation
import com.id.api.core.review.Review
import com.id.api.exceptions.InvalidInputException
import com.id.api.exceptions.NotFoundException
import com.id.microservices.composite.product.services.ProductCompositeIntegration
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.WebTestClient.BodyContentSpec
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toFlux
import reactor.kotlin.core.publisher.toMono

@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    classes = [TestSecurityConfig::class],
    properties = [
        "spring.security.oauth2.resourceserver.jwt.issuer-uri=",
        "spring.main.allow-bean-definition-overriding=true"
    ]
)
@ExtendWith(SpringExtension::class)
class ProductCompositeServiceApplicationTests(
    @Autowired private val client: WebTestClient
) {
    @MockkBean(relaxed = true)
    private lateinit var compositeIntegration: ProductCompositeIntegration

    @BeforeEach
    fun setUp() {

        every { compositeIntegration.getProduct(any(), PRODUCT_ID_OK, any(), any()) } returns Product(
            PRODUCT_ID_OK,
            "name",
            1,
            "mock-address"
        ).toMono()

        every { compositeIntegration.getRecommendations(any(), PRODUCT_ID_OK) } returns listOf(
            Recommendation(
                PRODUCT_ID_OK,
                1,
                "author",
                1,
                "content",
                "mock address"
            )
        ).toFlux()
        every { compositeIntegration.getReviews(any(), PRODUCT_ID_OK) } returns listOf(
            Review(
                PRODUCT_ID_OK,
                1,
                "author",
                "subject",
                "content",
                "mock address"
            )
        ).toFlux()

        every { compositeIntegration.getProduct(any(), PRODUCT_ID_NOT_FOUND, any(), any()) } throws NotFoundException("NOT FOUND: $PRODUCT_ID_NOT_FOUND")
        every { compositeIntegration.getProduct(any(), PRODUCT_ID_INVALID, any(), any()) } throws InvalidInputException("INVALID: $PRODUCT_ID_INVALID")
    }

    @Test
    fun getProductById() {
        getAndVerifyProduct(PRODUCT_ID_OK, HttpStatus.OK)
            .jsonPath("$.productId").isEqualTo(PRODUCT_ID_OK)
            .jsonPath("$.recommendations.length()").isEqualTo(1)
            .jsonPath("$.reviews.length()").isEqualTo(1)
    }

    @Test
    fun getProductNotFound() {
        getAndVerifyProduct(PRODUCT_ID_NOT_FOUND, HttpStatus.NOT_FOUND)
            .jsonPath("$.path").isEqualTo("/product-composite/$PRODUCT_ID_NOT_FOUND")
            .jsonPath("$.message").isEqualTo("NOT FOUND: $PRODUCT_ID_NOT_FOUND")
    }

    @Test
    fun getProductInvalidInput() {
        getAndVerifyProduct(PRODUCT_ID_INVALID, HttpStatus.UNPROCESSABLE_ENTITY)
            .jsonPath("$.path").isEqualTo("/product-composite/$PRODUCT_ID_INVALID")
            .jsonPath("$.message").isEqualTo("INVALID: $PRODUCT_ID_INVALID")
    }

    private fun getAndVerifyProduct(productId: Int, expectedStatus: HttpStatus): BodyContentSpec {
        return client.get()
            .uri("/product-composite/$productId")
            .accept(APPLICATION_JSON)
            .exchange()
            .expectStatus().isEqualTo(expectedStatus)
            .expectHeader().contentType(APPLICATION_JSON)
            .expectBody()
    }

    private fun postAndVerifyProduct(compositeProduct: ProductAggregate, expectedStatus: HttpStatus) {
        client.post()
            .uri("/product-composite")
            .body(Mono.just(compositeProduct), ProductAggregate::class.java)
            .exchange()
            .expectStatus().isEqualTo(expectedStatus)
    }

    private fun deleteAndVerifyProduct(productId: Int, expectedStatus: HttpStatus) {
        client.delete()
            .uri("/product-composite/$productId")
            .exchange()
            .expectStatus().isEqualTo(expectedStatus)
    }

    companion object {
        private const val PRODUCT_ID_OK = 1
        private const val PRODUCT_ID_NOT_FOUND = 2
        private const val PRODUCT_ID_INVALID = 3
    }
}
