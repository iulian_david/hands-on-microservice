package com.id.microservices.composite.product

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.id.api.composite.product.ProductAggregate
import com.id.api.composite.product.RecommendationSummary
import com.id.api.composite.product.ReviewSummary
import com.id.api.core.product.Product
import com.id.api.core.recommendation.Recommendation
import com.id.api.core.review.Review
import com.id.api.event.Event
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment
import org.springframework.cloud.stream.binder.test.OutputDestination
import org.springframework.cloud.stream.binder.test.TestChannelBinderConfiguration
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Import
import org.springframework.http.HttpStatus
import org.springframework.messaging.Message
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Mono

@SpringBootTest(
    webEnvironment = WebEnvironment.RANDOM_PORT,
    classes = [TestSecurityConfig::class],
    properties = [
        "spring.security.oauth2.resourceserver.jwt.issuer-uri=",
        "spring.main.allow-bean-definition-overriding=true",
        "spring.cloud.stream.defaultBinder=rabbit"
    ]
)
@Import(TestChannelBinderConfiguration::class)
internal class MessagingTests(
    @Autowired private val applicationContext: ApplicationContext,
    @Autowired private val target: OutputDestination
) {
    private lateinit var client: WebTestClient
    private val mapper = ObjectMapper().registerModule(KotlinModule.Builder().build())
    @BeforeEach
    fun setUp() {
        client = WebTestClient.bindToApplicationContext(applicationContext)
            .build()
        purgeMessages("products")
        purgeMessages("recommendations")
        purgeMessages("reviews")
    }

    @Test
    fun createCompositeProduct() {
        val composite = ProductAggregate(1, "name", 1, null, null, null)
        postAndVerifyProduct(composite, HttpStatus.ACCEPTED)

        val productMessages = getMessages("products")
        val recommendationMessages = getMessages("recommendations")
        val reviewMessages = getMessages("reviews")

        // Assert one expected new product event queued up
        assertEquals(1, productMessages.size)

        val expectedEvent: Event<Int, Product> = Event(
            Event.Type.CREATE,
            composite.productId,
            Product(composite.productId, composite.name, composite.weight, null)
        )
        compareEvents(productMessages, expectedEvent)

        // Assert no recommendation and review events
        assertEquals(0, recommendationMessages.size)
        assertEquals(0, reviewMessages.size)
    }

    @Test
    fun createCompositeProduct2() {
        val composite = ProductAggregate(
            1, "name", 1,
            listOf(RecommendationSummary(1, "a", 1, "c")),
            listOf(ReviewSummary(1, "a", "s", "c")), null
        )
        postAndVerifyProduct(composite, HttpStatus.ACCEPTED)

        val productMessages = getMessages("products")
        val recommendationMessages = getMessages("recommendations")
        val reviewMessages = getMessages("reviews")

        // Assert one create product event queued up

        // Assert one create product event queued up
        assertEquals(1, productMessages.size)

        val expectedProductEvent: Event<Int, Product> = Event(
            Event.Type.CREATE,
            composite.productId,
            Product(composite.productId, composite.name, composite.weight, null)
        )

        compareEvents(productMessages, expectedProductEvent)

        // Assert one create recommendation event queued up

        // Assert one create recommendation event queued up
        assertEquals(1, recommendationMessages.size)

        val rec: RecommendationSummary = composite.recommendations?.get(0)
            ?: fail("Should have at least 1 recommendation")
        val expectedRecommendationEvent: Event<Int, Recommendation> = Event(
            Event.Type.CREATE, composite.productId,
            Recommendation(
                composite.productId,
                rec.recommendationId,
                rec.author,
                rec.rate,
                rec.content,
                null
            )
        )

        compareEvents(recommendationMessages, expectedRecommendationEvent, "data.serviceAddress")

        // Assert one create review event queued up
        assertEquals(1, reviewMessages.size)

        val rev: ReviewSummary = composite.reviews?.get(0)
            ?: fail("should have at least 1 review")
        val expectedReviewEvent: Event<Int, Review> = Event(
            Event.Type.CREATE,
            composite.productId,
            Review(
                composite.productId,
                rev.reviewId,
                rev.author,
                rev.subject,
                rev.content,
                null
            )
        )

        compareEvents(reviewMessages, expectedReviewEvent)
    }

    private inline fun <reified T : Any> compareEvents(
        messages: List<String>,
        expectedEvent: Event<Int, T>,
        vararg vs: String
    ) {
        val newEvent = mapper.readValue(messages[0], object : TypeReference<Event<Int, T>>() {})
        assertThat(newEvent).usingRecursiveComparison()
            .ignoringFields(*vs).isEqualTo(expectedEvent)
    }

    @Test
    fun deleteCompositeProduct() {
        deleteAndVerifyProduct(1, HttpStatus.ACCEPTED)
        val productMessages = getMessages("products")
        val recommendationMessages = getMessages("recommendations")
        val reviewMessages = getMessages("reviews")

        // Assert one delete product event queued up
        assertEquals(1, productMessages.size)
        val expectedProductEvent: Event<Int, Product> = Event(Event.Type.DELETE, 1, null)

        compareEvents(productMessages, expectedProductEvent)

        // Assert one delete recommendation event queued up
        assertEquals(1, recommendationMessages.size)
        val expectedRecommendationEvent: Event<Int, Recommendation> = Event(Event.Type.DELETE, 1, null)
        compareEvents(recommendationMessages, expectedRecommendationEvent)

        // Assert one delete review event queued up
        assertEquals(1, reviewMessages.size)
        val expectedReviewEvent: Event<Int, Review> = Event(Event.Type.DELETE, 1, null)
        compareEvents(reviewMessages, expectedReviewEvent)
    }
    private fun purgeMessages(bindingName: String) {
        getMessages(bindingName)
    }

    private fun getMessages(bindingName: String): List<String> {
        val messages: MutableList<String> = ArrayList()
        var anyMoreMessages = true
        while (anyMoreMessages) {
            val message = getMessage(bindingName)
            if (message == null) {
                anyMoreMessages = false
            } else {
                messages.add(String(message.payload))
            }
        }
        return messages
    }

    private fun getMessage(bindingName: String): Message<ByteArray>? {
        return try {
            target.receive(0, bindingName)
        } catch (npe: NullPointerException) {
            // If the messageQueues member variable in the target object contains no queues when the receive method is called, it will cause a NPE to be thrown.
            // So we catch the NPE here and return null to indicate that no messages were found.
            LOG.error("getMessage() received a NPE with binding = {}", bindingName)
            null
        }
    }

    private fun postAndVerifyProduct(compositeProduct: ProductAggregate, expectedStatus: HttpStatus) {
        client.post()
            .uri("/product-composite")
            .body(Mono.just(compositeProduct), ProductAggregate::class.java)
            .exchange()
            .expectStatus().isEqualTo(expectedStatus)
    }

    private fun deleteAndVerifyProduct(productId: Int, expectedStatus: HttpStatus) {
        client.delete()
            .uri("/product-composite/$productId")
            .exchange()
            .expectStatus().isEqualTo(expectedStatus)
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(MessagingTests::class.java)
    }
}
